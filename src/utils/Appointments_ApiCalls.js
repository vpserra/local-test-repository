const axios = require('axios');

const handleAppointments = async (id, have_parent_class_prop) => {
  let appointmentFetchedData = [];
  const data2 = await handleFetchSchedule(id);
  const newData2 = have_parent_class_prop ? 
                    data2 : 
                    data2.filter(x => {
                      if(!"0".includes(x.properties.session_number)){
                        return x;
                      }
                    });
  await newData2 === "error" ? appointmentFetchedData = "error" : appointmentFetchedData.push(...newData2); 
  return await appointmentFetchedData;
}

const handleFetchSchedule = async (id) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/getClassesAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        id: id,
      }),
    };    
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  }
}

const handleRescheduleClass = async (id, old_makeup_schedule_id, new_makeup_schedule_id) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/rescheduleMakeUpClass',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        "contact_id": id,
        "old_makeup_schedule_id": old_makeup_schedule_id,
        "new_makeup_schedule_id": new_makeup_schedule_id
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  } 
}

const handleFetch_Location = async(class_id) => {
  try{
    let config = {
      method: 'POST',
      url: '/_hcms/api/getClassLocation',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        class_id
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response;
  }catch{
    return 'error';
  }
}

export { handleFetch_Location, handleAppointments, handleFetchSchedule, handleRescheduleClass };
