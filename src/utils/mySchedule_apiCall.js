import axios from 'axios';
import moment from 'moment';
import { validSchedule, timeFormatted, dateFormatMDY } from '../utils/mySchedule_functions';

const getAppointmentsDataFn = async (id, product_status) => {
  let appointmentFetchedData = [];
  const data1 = product_status["Driving Lessons"].status ? await handleAppointment({id, slot_type: "Driving Lesson"}) : [];
  const data2 = product_status["License Testing"].status ? await handleAppointment({id, slot_type: "Road Test"}) : [];
  if(data1 === "error" || data2 === "error"){
    appointmentFetchedData = "error";
  }else{
    await appointmentFetchedData.push(...data1);
    await appointmentFetchedData.push(...data2);
  } 
  return await appointmentFetchedData;
}

const handleAppointment = async ({id, slot_type}) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/getAssocContactToAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        id: id,
        slot_type: slot_type
      }),
    };    
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  }
};

const handle_schedule_data_manipulate = (rawdata, hasParentClassProp, type) => {
  const data = [];

  if(type === "all"){
    rawdata.schedule.forEach( schedule_row => data.push({class: rawdata.class, schedule: schedule_row}));
    rawdata.MakeUpClasses.forEach( makeup_row => {
      makeup_row.makeup_schedule.length !== 0 && makeup_row.makeup_schedule.forEach( makeup_schedule_row => {
          data.push({makeup_class: makeup_row.makeup_class, makeup_schedule: makeup_schedule_row})
      });        
    });
  }else{
    rawdata.schedule.forEach((schedule_row) => {
      if (rawdata.MakeUpClasses.length !== 0) {
        const session_data_array = [];

        rawdata.MakeUpClasses.forEach((makeup_row) => {
          makeup_row.makeup_schedule.forEach((makeup_schedule_row) => {
            if (
              schedule_row.properties.session_number ===
                makeup_schedule_row.properties.session_number &&
              !makeup_schedule_row.properties.hasOwnProperty("status")
            ) {
              session_data_array.push({
                makeup_class: makeup_row.makeup_class,
                makeup_schedule: makeup_schedule_row,
              });
            }
          });
        });

        if (session_data_array.length !== 0) {
          const latest_schedule =
            session_data_array[session_data_array.length - 1];
          if (
            validSchedule({
              date: latest_schedule.makeup_schedule.properties.schedule_date,
              time:
                latest_schedule.makeup_schedule.properties.schedule_start_time,
            })
          ) {
            data.push(latest_schedule);
          }
        } else if (
          session_data_array.length === 0 &&
          validSchedule({
            date: schedule_row.properties.schedule_date,
            time: schedule_row.properties.schedule_start_time,
          }) &&
          !schedule_row.properties.hasOwnProperty("status")
        ) {
          data.push({ class: rawdata.class, schedule: schedule_row });
        }
      } else {
        if (
          validSchedule({
            date: schedule_row.properties.schedule_date,
            time: schedule_row.properties.schedule_start_time,
          }) &&
          !schedule_row.properties.hasOwnProperty("status")
        ) {
          data.push({ class: rawdata.class, schedule: schedule_row });
        }
      }
    });
  }

  return hasParentClassProp
    ? data
    : data.filter((x) => {
        if (
          !"0".includes(
            x.class
              ? x.schedule.properties.session_number
              : x.makeup_schedule.properties.session_number
          )
        ) {
          return x;
        }
      });
};

const getSchedulesDataFn = async id => {
  try {
    let config = {
      method: "POST",
      url: "/_hcms/api/studentSchedule",
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        contact_id: id,
      }),
    };
    const getResult = await axios(config);
    const rawdata = await getResult.data.response.status || await getResult.data.response;

    return await rawdata === "No Class"
      ? "No Class"
      : rawdata;
  } catch (error) {
    return "error";
  }
};

const getAvailableMakeupScheduleFn = async (state, schedules, date_param) => {

  const epochDate = +moment(date_param);

  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/makeUpClassesList',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({
        state: state,
        status: "Open",
        current_date: epochDate,
      }),
    };
    const getResult = await axios(config);  
    const rawdata = await getResult.data.response;

    const schedResult = await [];

    for (let available_row of rawdata) {
      if (
        available_row.properties.schedule_date &&
        available_row.properties.schedule_start_time &&
        available_row.properties.schedule_end_time
      ) {
        const withConflict = await time_conflict_checker(available_row, schedules);
        withConflict.length === 0 && schedResult.push(available_row);
      }
    }

    return await schedResult;
  } catch (error) {
    return 'error';
  }
}

const time_conflict_checker = (available_row, myschedule) => {
  var ar_date = dateFormatMDY(available_row.properties.schedule_date);
  var ar_start_time = timeFormatted(
    available_row.properties.schedule_start_time
  );
  var ar_end_time = timeFormatted(available_row.properties.schedule_end_time);
  return myschedule.filter((el) => {
    var el_date;
    var el_start_time;
    var el_end_time;
    var el_id;

    if (el.type) {
      el_date = dateFormatMDY(el.properties.schedule_date);
      el_start_time = timeFormatted(el.properties.schedule_start_time);
      el_end_time = timeFormatted(el.properties.schedule_end_time);
      el_id = el.id;
    } else {
      el_date = dateFormatMDY(
        el.properties
          ? el.properties.appointment_date
          : el.class
          ? el.schedule.properties.schedule_date
          : el.makeup_schedule.properties.schedule_date
      );
      el_start_time = timeFormatted(
        el.properties
          ? el.properties.appointment_start_time
          : el.class
          ? el.schedule.properties.schedule_start_time
          : el.makeup_schedule.properties.schedule_start_time
      );
      el_end_time = timeFormatted(
        el.properties
          ? el.properties.appointment_end_time
          : el.class
          ? el.schedule.properties.schedule_end_time
          : el.makeup_schedule.properties.schedule_end_time
      );
      el_id = el.properties
        ? el.id
        : el.class
        ? el.schedule.properties.id
        : el.makeup_schedule.properties.id;
    }

    if (
      available_row.id === el_id ||
      (ar_date === el_date &&
        ((el_start_time >= ar_start_time && el_start_time <= ar_end_time) ||
          (el_end_time >= ar_start_time && el_end_time <= ar_end_time)))
    ) {
      return available_row;
    }
  });
};

const reschedAppointmentFn = async ({id,slot_type, old_appointment_id, new_appointment_id}) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/rescheduleAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        contact_id: id,
        slot_type,
        old_appointment_id: old_appointment_id,
        new_appointment_id: new_appointment_id
      }),
    };
    const getResult = await axios(config);
    return await getResult.data;
  } catch (error) {
    return 'error';
  }
}

const cancelAppointmentFn = async (id, appointment_id) =>{
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/cancelAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        id: id,
        appointment_id: appointment_id        
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response
  } catch (error) {
    return await "error";
  }
}

const handleAppointmentsFn = async (type, state, schedules) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/filterAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        "slot_type": type,
        "state": state
      }),
    };
    const getResult = await axios(config);
    const rawdata = await getResult.data.response;

    const schedResult = await [];

    for (let available_row of rawdata) {
      if (
        available_row.properties.appointment_date &&
        available_row.properties.appointment_start_time &&
        available_row.properties.appointment_end_time
      ) {
        const withConflict = await time_conflict_checker(available_row, schedules);
        withConflict.length === 0 && schedResult.push(available_row);
      }
    }

    return await schedResult;

  } catch (error) {
    return 'error';
  }
}

const reschedMakeupClass = async (id, old_makeup_schedule_id, new_makeup_schedule_id) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/rescheduleMakeUpClass',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        "contact_id": id,
        "old_makeup_schedule_id": old_makeup_schedule_id,
        "new_makeup_schedule_id": new_makeup_schedule_id
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  }
}

const handleFetch_Locations = async (state) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/getLocationFilter',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        "state": state
      }),
    };
    const rawdata = await axios(config);
    const getResult = rawdata.data.response.filter(x => x.properties.location_name !== "Live Virtual Room CR");
    return await getResult;
  } catch (error) {
    return 'error';
  }
}

const handle_cancel_appointment_stripe = async (stripe_customer_id, amount) => {
  try {
    let config = {
      method: 'POST',
      url: 'https://hook.integromat.com/se6fh7a4pxa5vojmnocch8ka8blf5t9x',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        stripe_customer_id,
        amount
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  }
}

const handle_cancel_schedule = async (contact_id, schedule_id) => {
  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/cancelSchedule',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        contact_id,
        schedule_id
      }),
    };
    const getResult = await axios(config);
    return await getResult.data.response
  } catch (error) {
    return await "error";
  }
}

export { handle_cancel_schedule, handle_cancel_appointment_stripe, handle_schedule_data_manipulate, handleAppointmentsFn, handleFetch_Locations, handleAppointment, getAppointmentsDataFn, getSchedulesDataFn, getAvailableMakeupScheduleFn, reschedAppointmentFn, cancelAppointmentFn, reschedMakeupClass };