import Geocode from "react-geocode";
// import axios from 'axios'
// import moment from 'moment'

Geocode.setApiKey("AIzaSyAO7JsEJ7Id9IVF0AaoRRAY3A5iiekVjZE");

function distance(lat1, lon1, lat2, lon2, unit, list) {
  if (lat1 === lat2 && lon1 === lon2) {
    return {
      distance: 0,
      data: list,
    };
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === "K") {
      dist = dist * 1.609344;
    }
    if (unit === "N") {
      dist = dist * 0.8684;
    }
    return {
      distance: dist,
      data: list,
    };
  }
}

const getGeoLocation = async (location) => {
  const handleGeo = await Geocode.fromAddress(location),
    { lat, lng } = await handleGeo.results[0].geometry.location;
  return {
    lat: lat,
    lng: lng,
  };
};

const handleGeo = async (location, list) => {
  const geoData = await getGeoLocation(location);
  if (geoData instanceof Error) {
    await handleGeo(location, list);
  } else {
    return {
      geoLocation: geoData,
      list: list,
    };
  }
};
const handleLocation = async (list) => {
  const setLocationData = await Promise.all(
    list.map(async (location) => {
      let locProp = location.properties;
      return await handleGeo(locProp.full_address, location);
    })
  );
  return await setLocationData; 
};
const handleDistance = async (lat, lng, list) => {
//  const handleCoordinated = await handleLocation(list);
  const getDistance = await list.map((data) => {
    return distance(
      lat,
      lng,
      data.properties.latitude, 
      data.properties.longitude,
      "M",
      data
    );
  });
  return await getDistance;
};

export { handleDistance, getGeoLocation };