import moment from 'moment-timezone';
import Geocode from "react-geocode";
const dateRangeArray = require('date-range-array');

const getTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
const enrollmentDate = v => v ? moment(v).tz(getTimezone).format('MM/DD/YYYY z') : moment(new Date()).tz(getTimezone).format('MM/DD/YYYY z');
const dateFormatted = v => moment(v).format("ddd, MMM D YYYY");
const dateCountdown = ({ date, time }) => {
  const mergedDate = [date,time].join(" ");
  let duration = moment(mergedDate) - moment().local();
  let minutes = Math.floor( (duration/1000/60) % 60 );
  let hours = Math.floor( (duration/(1000*60*60)) % 24 );
  let days = Math.floor( duration/(1000*60*60*24) );
  const duration_text = `${days !== 0 ? days > 1 ? `${days} days `: `${days} day ` : ''}${hours !== 0 ? hours > 1 ? `${hours} hours ` : `${hours} hour ` : ''}${minutes !== 0 ? minutes > 1 ? `${minutes} minutes` : `${minutes} minute`: ''}`;
  return duration_text;
};
const dayFormatted = v => moment(v).format("ddd");
const compareDate = v => moment(v).format("L") > moment(new Date()).format("L");
const dateFormatMDY = v => moment(v).format('ll');
const dateFormatMDYHMS = ({date, time}) => moment([date,time].join(" ")).format("L LT");
const dateFormatPicker = (v) => moment(v).format("L");
const dateRangeArrayFormat = (v) => moment(v).format("YYYY-MM-DD");
const add37Days = v => new Date(new Date().setDate(new Date(dateFormatMDY(v)).getDate() + 37));
const addDays = (date,numberOfDays) => new Date(moment(date).add(numberOfDays,'days').tz(getTimezone).format('MM/DD/YYYY z'));
const addMonths = (date,numberOfMonths) => new Date(moment(date).add(numberOfMonths,'months').tz(getTimezone).format('MM/DD/YYYY z'));
const disabledStartDate = new Date();
const disabledEndDate = new Date(addMonths(new Date(), 8));
const dayDifference = (start,end) => {
  const a = moment(start);
  const b = moment(end);
  return b.diff(a, 'days');
}

const disabledDatesFn = (state) => {
  let arr = state;
  const disabledRangeDate = dateRangeArray(dateRangeArrayFormat(disabledStartDate),dateRangeArrayFormat(disabledEndDate)); 
  arr.forEach(scheduleRow => {
    if(scheduleRow.properties){
      const matchedDateIndex = disabledRangeDate.indexOf(dateRangeArrayFormat(scheduleRow.properties.appointment_date));
      matchedDateIndex !== -1 && disabledRangeDate.splice(matchedDateIndex, 1);
    }else{
      const matchedDateIndex = disabledRangeDate.indexOf(dateRangeArrayFormat(scheduleRow.makeup_schedule.properties.schedule_date));
      matchedDateIndex !== -1 && disabledRangeDate.splice(matchedDateIndex, 1);
    }
  });
  return disabledRangeDate.map(dateRow => new Date(enrollmentDate(dateRow)) );
}
const defaultDisabledDates = dateRangeArray(dateRangeArrayFormat(disabledStartDate),dateRangeArrayFormat(disabledEndDate)).map(dateRow => new Date(dateRow));

const handleFilters = (schedule, filter) => {
  if (schedule.properties) {
    return ((filter === 'license testing' && schedule.properties.slot_type.toLowerCase() === 'road test') || (filter === schedule.properties.slot_type.toLowerCase())) && schedule
  } else {
    return (filter === 'classes') && schedule
  }
}

const handleFiltersWithDate = (schedule, filter, dateSelected) => {
  if (schedule.properties) {
    return (((filter === 'license testing' && schedule.properties.slot_type.toLowerCase() === 'road test') || (filter === schedule.properties.slot_type.toLowerCase())) && dateRangeArrayFormat(dateSelected) === dateRangeArrayFormat(schedule.properties.appointment_date)) && schedule
  } else {
    return (filter === 'classes' && dateRangeArrayFormat(dateSelected) === dateRangeArrayFormat(schedule.class ? schedule.schedule.properties.schedule_date : schedule.makeup_schedule.properties.schedule_date)) && schedule
  }
}

const handleMainFilter = ({scheduleState, activeFilterValues, dateSearching, dateSelected}) => {
  return scheduleState.filter(schedule => {
    var filteredData; 
    if(activeFilterValues){
      if(dateSearching){
        filteredData = activeFilterValues.filter( filter => handleFiltersWithDate(schedule, filter, dateSelected));
      }else{
        filteredData = activeFilterValues.filter( filter => handleFilters(schedule, filter));
      }	
    }else{
      if(dateSearching){
        if(schedule.properties){
          return (dateRangeArrayFormat(dateSelected) === dateRangeArrayFormat(schedule.properties.appointment_date)) && schedule;
        }else if(schedule.makeup_class){
          return (dateRangeArrayFormat(dateSelected) === dateRangeArrayFormat(schedule.makeup_schedule.properties.schedule_date)) && schedule;
        }else if(schedule.class){
          return (dateRangeArrayFormat(dateSelected) === dateRangeArrayFormat(schedule.schedule.properties.schedule_date)) && schedule;
        }
      }else{
        filteredData = schedule;
      }
    }
    
    return filteredData.length !== 0;
  });
}

const getMonth = v => moment(v).format("MMMM");

const search_timeFormat = v => moment(v, 'h:mm a').format('HH:mm');
const timeFormatted = v => moment(v,'h:mm a').format("LT");

const monthDay_Format = v => moment(v).format("MMMM D");

const dateFormat = (v) => ( moment(v).tz(getTimezone).format('l LT z') );

const validSchedule = ({date, time}) => {
  return new Date(dateFormat(new Date([date,time].join(" ")))) > new Date(dateFormat(new Date()))
};

const handle_cancellation = ({date, time, hour}) => {
  const v = moment(dateFormatMDYHMS({date,time}))
  return( v.diff(moment(new Date()), 'hours', true) < hour );
}

const calendar_formatter = v => v.format("L");

Geocode.setApiKey("AIzaSyAO7JsEJ7Id9IVF0AaoRRAY3A5iiekVjZE");

const getGeoLocation = async (location) => {
    const handleGeo = await Geocode.fromAddress(location),
      { lat, lng } = await handleGeo.results[0].geometry.location;
    return {
      lat: lat,
      lng: lng,
    };
};
const distance = (lat1, lon1, lat2, lon2, unit, data) => {
    if (lat1 === lat2 && lon1 === lon2) {
      return {
        distance: 0,
        data: data
      };
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit === "K") {
        dist = dist * 1.609344;
      }
      if (unit === "N") {
        dist = dist * 0.8684;
      }
      return {
        distance: dist,
        data: data
      };
    }
}

const format_timezone = v => {
  return v ? 
  v.split("_slash_")
  .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
  .join("/") : 
  getTimezone;
}

const epoch_date = (timezone) => moment().tz(timezone).format('l');

export { format_timezone, epoch_date, getGeoLocation, distance, calendar_formatter, handle_cancellation, validSchedule, dateFormat, monthDay_Format, timeFormatted, search_timeFormat, getMonth, dayDifference, addDays, addMonths, handleMainFilter, dateFormatted, dateCountdown, dayFormatted, compareDate, dateFormatMDY, dateFormatMDYHMS, dateFormatPicker, disabledDatesFn, dateRangeArrayFormat, defaultDisabledDates, add37Days, enrollmentDate};