import moment from 'moment';
import React from 'react';
import { Link as MuiLink } from '@material-ui/core';

const dateFormatted = v => moment(v).format("ddd, ll");
const timeFormatted = v => moment(v,'h:mm a').format("LT");
const dateFormatMDYHM = (date, time) => moment([date,time].join(" ")).format("l LT");
const dateFormattedNoComma = v => moment(v).format("ddd ll");
const dateCountdown = ({ date, time }) => {
  const mergedDate = [date,time].join(" ");
  let duration = moment(mergedDate) - moment().local();
  let minutes = Math.floor( (duration/1000/60) % 60 );
  let hours = Math.floor( (duration/(1000*60*60)) % 24 );
  let days = Math.floor( duration/(1000*60*60*24) );
  return (`${days} days ${hours} hours ${minutes} minutes`);   
};
const dateMDDYYYY = v => moment(v).format("L");
const compareDate = (date,time) => dateMDDYYYY(dateFormatMDYHM(date,time)) > dateMDDYYYY(new Date());

const urlChecker = value => {
  var regex = new RegExp(/[-a-zA-Z0-9@:%._\\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)?/gi);
  if(value || value === ""){
    if(value.match(regex)){
      return (
        <MuiLink
          href={value}
          underline="hover"
          target="_blank" 
          rel="noopener noreferrer"
        >
          {value}
        </MuiLink>)
    }
    return value;
  }else{
    return "TBD"
  }
}

export {dateFormatted, timeFormatted, dateFormatMDYHM, dateFormattedNoComma, dateCountdown, compareDate, urlChecker, dateMDDYYYY}