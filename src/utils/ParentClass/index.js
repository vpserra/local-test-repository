import axios from 'axios';
import moment from 'moment';

const fetch_open_parent_class = async (state, date_param) => {

  const epochDate = +moment(date_param);

  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/parent_class',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({ 
        state,
        date: epochDate,
        type: "schedule"
      }),
    };    
    const getResult = await axios(config);
    return await getResult.data.response.results;
  } catch (error) {
    return "error"
  }
}

const schedule_parent_class = async (contact_id, schedule_id) => {

  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/scheduleParentClass',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({
        contact_id,
        schedule_id
    }),
    };    
    const getResult = await axios(config);
    console.log(getResult.data)
    return await getResult.data.response;
  } catch (error) {
    return "error"
  }
}

const fetch_scheduled_parent_class = async (id) => {

  try {
    let config = {
      method: 'POST',
      url: '/_hcms/api/getParentClassAppointment',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({id}),
    };    
    const getResult = await axios(config);
    return await getResult.data.response;
  } catch (error) {
    return "error"
  }

}

export {
  fetch_scheduled_parent_class,
  fetch_open_parent_class,
  schedule_parent_class
};