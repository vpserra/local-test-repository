export function getLocations() {
  let getLocationsOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  let getAllLocations = fetch('_hcms/api/getLocations', getLocationsOptions);
  let getAllLocationsJson = getAllLocations.json();
  return getAllLocationsJson;
}

const filterAppointmentDatasDrivingLesson = async state => {
  try {
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    let raw = JSON.stringify({
      slot_type: 'Driving Lesson',
      state: state
    });

    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw
    };

    const appointmentDatas = await fetch(
      '_hcms/api/filterAppointment',
      requestOptions
    );
    const appointmentDatasJson = await appointmentDatas.json();
    return appointmentDatasJson.response;
  } catch (err) {
    console.error(err.message);
    return 'error'
  }
};

const filterAppointmentDatasRoadTest = async state => {
  try {
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    let raw = JSON.stringify({
      slot_type: 'Road Test',
      state: state
    });
    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw
    };
    const appointmentDatas = await fetch(
      '_hcms/api/filterAppointment',
      requestOptions
    );
    const appointmentDatasJson = await appointmentDatas.json();
    return appointmentDatasJson.response;
  } catch (err) {
    console.error(err.message);
    return 'error'
  }
};

const getRegisteredEvents = async () => {
  // This function fetchs same-origin in order to pass cookies to the API and recieves a formSubmissions object
  let response = await fetch(`/_hcms/api/membership`, {
    method: 'GET',
    mode: 'same-origin',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  response = await response.json();
  return response;
};

const getUserInfo = async contactId => {
  let myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  let raw2 = JSON.stringify({
    id: contactId
  });

  let requestOptions2 = {
    method: 'POST',
    headers: myHeaders,
    body: raw2
  };

  // get student info using the user id from the token
  const studentInfo = await fetch('_hcms/api/getStudentInfo', requestOptions2);
  const studentInfoJson = await studentInfo.json();
  const userDatas = studentInfoJson.response.properties;
  return userDatas;
};

const DrivingLessonsAppointment = async ({ id }) => {
  try {
    let config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        id: id,
        slot_type: 'Driving Lesson'
      })
    };
    const getResult = await fetch(
      '/_hcms/api/getAssocContactToAppointment',
      config
    );
    return await getResult.data.response;
  } catch (error) {
    return 'error';
  }
};

const getCardDetails = async (contactId, stripe_id) => {
  let myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  let raw5 = JSON.stringify({
    contact_id: contactId,
    stripe_customer_id: stripe_id
  });

  let cardOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw5
  };

  const cardDetails = await fetch(
    'https://hook.integromat.com/5yj2wu9dcj1sn5d71ixokbd7jekbpo6t', //dev and staging
    // 'https://hook.integromat.com/y8gy3xpqhkihe7a4mzlujppuz9y135b2',//production
    cardOptions
  ); 
  const cardDetailsJson = await cardDetails.json();
  return cardDetailsJson;
};

const canScheduleLesson = async contactId => {
  let myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  let raw6 = JSON.stringify({
    contact_id: contactId
  });

  let cardOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw6
  };

  const purchasedProduct = await fetch(
    '/_hcms/api/getProductCompletion',
    cardOptions
  );
  const purchasedProductJson = await purchasedProduct.json();
  return purchasedProductJson.response;
};

const schedAppointmentFn = async (contact_id, appointment_id) => {
  try {
    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        contact_id: contact_id,
        appointment_id: appointment_id
      })
    };
    const getResult = await fetch(
      '_hcms/api/scheduleAppointment',
      requestOptions
    );
    const getResultJson = await getResult.json();
    return await getResultJson.data;
  } catch (err) {
    console.error(err.message);
  }
};

const updateCompletionStatus = async (contact_id, product_id) => {
  try {
    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        contact_id: contact_id,
        product_id: product_id
      })
    };
    const getResultCancelTest = await fetch(
      '_hcms/api/updateCompletionStatus',
      requestOptions
    );
    const cancelTestJson = await getResultCancelTest.json();
    return cancelTestJson;
  } catch (err) {
    console.error(err.message);
  }
};

export {
  getUserInfo,
  getCardDetails,
  updateCompletionStatus,
  canScheduleLesson,
  schedAppointmentFn,
  getRegisteredEvents,
  DrivingLessonsAppointment,
  filterAppointmentDatasRoadTest,
  filterAppointmentDatasDrivingLesson
};