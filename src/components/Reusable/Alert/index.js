import React from 'react';
import { 
  Warning as WarningIcon, 
  CheckCircle as CheckCircleIcon
} from '@material-ui/icons';
import { Dialog,DialogContent,DialogActions,Button,Typography } from '@material-ui/core';

const Alert = (props) => {
  // console.log('Alert.js')
  const { handleClose, open, alertProps} = props;
  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogContent sx={{minWidth: "480px"}}>
        <div sx={{height: 200, display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
          <div>
            {(alertProps.status === "error") ?
              <WarningIcon sx={{fontSize: 70, color: alertProps.color}} />:
              <CheckCircleIcon sx={{fontSize: 70, color: alertProps.color}} />
            }
          </div>
          <div>
            <Typography sx={{fontWeight: 500, fontSize: 30, textAlign: "center", color: "#445766"}}>
              {alertProps.text || ""}
            </Typography>
          </div>
          <div>
            <Typography sx={{fontSize: 20, textAlign: "center", color: "#445766"}}>
              {alertProps.text1 || ""}    
            </Typography>
          </div>
          <div>
            <Typography sx={{fontSize: 20, textAlign: "center", color: "#445766"}}>
              {alertProps.text2 || ""}    
            </Typography>
          </div>
        </div>
      </DialogContent>
      <DialogActions sx={{padding: "0"}}>
        <Button onClick={handleClose} sx={{width: "100%", borderRadius: 0, backgroundColor: alertProps.color, height: 50, color: "#ffffff", fontWeight: 500}} variant="contained">Dismiss</Button>
      </DialogActions>
    </Dialog>
  )
}

export default React.memo(Alert);
