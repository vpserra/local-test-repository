import React, { Fragment } from 'react';

import {
  Col
} from 'react-bootstrap';
import {
  TextField
} from '@material-ui/core';

const DateRange = ({filter_query, handleChangeFilter}) => {
  return (
    <Fragment>
      <Col lg={6} md={6} sm={6} xs={12}>
        <div className="filter-field-container">
          <TextField
            variant="outlined"
            size="small"
            fullWidth
            id="date"
            label="Date Start"
            onChange={handleChangeFilter("startDate")}
            value={filter_query.initial.startDate || ""}
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              min: filter_query.defaultValue.minDate,
              max: filter_query.defaultValue.maxDate
            }}
            />
        </div>
      </Col>
      <Col lg={6} md={6} sm={6} xs={12}>
        <div className="filter-field-container">
          <TextField
            variant="outlined"
            size="small"
            fullWidth
            id="date"
            label="Date End"
            onChange={handleChangeFilter("endDate")}
            value={filter_query.initial.endDate || ""}
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              min: filter_query.initial.startDate || filter_query.defaultValue.minDate,
              max: filter_query.defaultValue.maxDate
            }}
            />
        </div>
      </Col>
    </Fragment>
  )
}

export default React.memo(DateRange);
