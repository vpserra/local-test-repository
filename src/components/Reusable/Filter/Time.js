import React from 'react';
import {
  TextField
} from '@material-ui/core';

const Time = ({filter_query, handleChangeFilter }) => {
  return (
    <TextField
      variant="outlined"
      size="small"
      fullWidth
      id="time"
      label="Start time"
      type="time"
      value={filter_query.initial.time || ""}
      onChange={handleChangeFilter("time")}
      InputLabelProps={{
        shrink: true,
      }}
      inputProps={{
        step: 300, // 5 min
      }}
    />
  )
}

export default Time
