import React from 'react';

import {
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';

const LocationProximity = ({filter_query, handleChangeFilter}) => {
  
  return (
    <FormControl variant="outlined" sx={{ m: 1, width: "100%" }}>
      <InputLabel id="filter-distance-proximity-label">Distance</InputLabel>
      <Select
        labelId="filter-distance-proximity-label"
        id="filter-distance-proximity"
        value={filter_query.initial.proximity}
        onChange={handleChangeFilter("proximity")}
        label="Distance"
      > 
        <MenuItem value={0}>Select Distance</MenuItem>
        <MenuItem value={5}>5 miles</MenuItem>
        <MenuItem value={10}>10 miles</MenuItem>
        <MenuItem value={15}>15 miles</MenuItem>
        <MenuItem value={30}>30 miles</MenuItem>
        <MenuItem value="all">Entire State</MenuItem>
      </Select>
    </FormControl>
  )
}

export default LocationProximity
