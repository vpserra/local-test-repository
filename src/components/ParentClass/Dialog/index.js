import React, { Fragment, useContext } from 'react';
import {
  Dialog,
  DialogTitle as MuiDialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Button,
  Typography,
  styled
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {
  Close as CloseIcon
} from '@material-ui/icons';
// import Loader from '../../Loader';

import { ParentClassContext } from '../../../Context/ParentClass';

import { 
  dateFormatPicker, 
  timeFormatted 
} from '../../../utils/mySchedule_functions';

import AddSchedule from "./AddSchedule";
import Schedule from "./Schedule/";
import Upload from "./Upload/";
import Status from "./Status";
import Cancel from "./Cancel";
import Details from "./Details";

const DialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <MuiDialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

DialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

const ParentClassDialog = () => {

  const { parent_class_actions, handle_modal_content, handle_close_dialog, selected_data } = useContext(ParentClassContext);

  const DialogContentOutput = ({value}) => {
    switch (value) {
      case 0:
        return <AddSchedule />;
      case 1:
        return <Schedule type="schedule" />;
      case 3:
        return <Schedule type="reschedule" />;
      case 2: 
        return <Upload />;
      case 4:
        return <Status />;
      case 5:
        return <Cancel />;
      case 6:
        return <Details />;
      default:
        break;
    }
  }

  const handle_dialog_width = (value) => {
    switch (value) {
      case 1: 
      case 3:
        return "md";
      default:
        return "sm";
    }
  }
  
  const handle_dialog_button_text = (value) => {
    switch (value) {
      case 1:
      case 2: 
      return "Go Back";
    default: 
      return "Close";
    }
  }

  const handle_dialog_button = (value) => {
    switch (value) {
      case 1:
      case 2:
        return () => handle_modal_content(0);
      default:
        return handle_close_dialog;
    } 
  }

  const handle_dialog_header_text = (value) => {
    switch (value) {
      case 1:
        return "Schedule Parent Class"
      case 2:
        return "Upload Certificate";
      case 3: 
        return "Reschedule Parent Class";
      case 4:
        return "Parent Class Status";
      case 5:
        return (
          <Fragment>
            <Typography className="myschedule-cancel-text">Are you sure you want to cancel this class?</Typography>
            <Typography className="myschedule-cancel-text">
              Lesson Date: {
                dateFormatPicker(selected_data.schedule?.properties.schedule_date || "")
              } {
                timeFormatted(selected_data.schedule?.properties.schedule_start_time || "")
              } - {
                timeFormatted(selected_data.schedule?.properties.schedule_end_time || "")
              }
            </Typography>
          </Fragment>
        );
      case 6:
        return "Parent Class Details";
      default: 
        return "Parent Class";
    }
  }
  
  const DialogButtons = ({value}) => {
    switch(value){
      case 2:
        return (
          <Fragment>
            <Button
              id="upload-btn"
              // onClick={handle_dialog_button(parent_class_actions.modal_content)}
              variant="contained"
              className="green-tns btn-shadow"
            >
              Upload
            </Button>
            <Button
              id="close-btn"
              onClick={handle_dialog_button(parent_class_actions.modal_content)}
              variant="contained"
              className="orange-tns btn-shadow"
            >
              {handle_dialog_button_text(parent_class_actions.modal_content)}
            </Button>
          </Fragment>
        );
      default: 
        return (
          <Button
            id="close-btn"
            onClick={handle_dialog_button(parent_class_actions.modal_content)}
            variant="contained"
            className="orange-tns close-btn btn-shadow"
          >
            {handle_dialog_button_text(parent_class_actions.modal_content)}
          </Button>
        )
    }
  }

  //handle for not showing dialog actions for cancellation design
  const ShowDialogActions = ({value}) => {
    if(value !== 5){
      return (
        <DialogActions>
          <DialogButtons value={parent_class_actions.modal_content}/>
        </DialogActions>
      );
    }
    return "";
  }

  return (
    <Fragment>
      <Dialog
        onClose={handle_close_dialog}
        aria-labelledby="parent-class-dialog"
        open={parent_class_actions.schedule_dialog}
        maxWidth={handle_dialog_width(parent_class_actions.modal_content)}
        fullWidth
      >
        <DialogTitle 
          onClose={handle_close_dialog}
        > 
          {handle_dialog_header_text(parent_class_actions.modal_content)}
        </DialogTitle>
        <DialogContent dividers>          
          <DialogContentOutput value={parent_class_actions.modal_content} />
        </DialogContent>
        <ShowDialogActions value={parent_class_actions.modal_content} />
      </Dialog>
    </Fragment>
  )
}

export default React.memo(ParentClassDialog);
