import React, { useContext } from 'react';

import {
  Button 
} from "@material-ui/core";

import { ParentClassContext } from '../../../../Context/ParentClass';

const Cancel = () => {

  const { handle_close_dialog, handle_cancel_parent_class } = useContext(ParentClassContext);

  return (
    <div className="cancel-parent-class-button-container">
      <Button
        id="cancel-lesson-btn"
        variant="contained"
        className="green-tns btn-shadow"
        onClick={() => handle_cancel_parent_class()}
      >
        Yes, Cancel class
      </Button>
      <Button
        id="close-btn"
        variant="contained"
        className="orange-tns btn-shadow"
        onClick={handle_close_dialog}
      >
        No, Do not cancel
      </Button>
    </div>
  )
}

export default React.memo(Cancel);
