import React, { useContext, Fragment } from 'react';
import {Row, Col} from 'react-bootstrap'
import { Button } from '@material-ui/core';
import { ParentClassContext } from '../../../../Context/ParentClass';

const AddSchedule = () => {

  const { handle_modal_content } = useContext(ParentClassContext);

  return (
    <Fragment>
      <Row>
        <Col>
          <div className="parent-class-choose-container">
            <Button 
              sx={{textAlign: 'center'}} 
              fullWidth
              id="have-parent-class-btn"
              className="green-tns btn-shadow"
              onClick={ () => handle_modal_content(2) }
            >
              I ALREADY TOOK THE PARENT CLASS
            </Button>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="parent-class-choose-container">
            <Button 
              sx={{textAlign: 'center'}} 
              fullWidth
              className="green-tns btn-shadow"
              id="schedule-parent-class-btn"
              onClick={ () => handle_modal_content(1) }
            >
              SCHEDULE YOUR PARENT CLASS WITH THE NEXT STREET
            </Button>
          </div>
        </Col>
      </Row>
    </Fragment>
  )
}

export default React.memo(AddSchedule);
