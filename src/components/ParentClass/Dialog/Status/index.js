import React, { useContext } from 'react';
import { Row, Col } from "react-bootstrap";
import{
  Typography
} from "@material-ui/core";

import { ParentClassContext } from '../../../../Context/ParentClass';

import {
  dateFormatted,
  timeFormatted,
} from "../../../../utils/Appointments_functions";

const Status = () => {

  const { selected_data } = useContext(ParentClassContext);

  console.log(dateFormatted(selected_data.schedule?.properties.schedule_date) || "")

  const detailsData = [
    { name: "Date:", value: dateFormatted(selected_data.schedule?.properties.schedule_date) || ""},
    { name: "Start Time:", value: timeFormatted(selected_data.schedule?.properties.schedule_start_time) || "" },
    { name: "Status:", value: selected_data.schedule?.status ? selected_data.schedule.status.charAt(0).toUpperCase() + selected_data.schedule.status.slice(1).toLowerCase() : ""},
    { name: "Notes:", value: selected_data.schedule?.class_properties.description || "" },
  ];

  return (
    <Row>
      <Col>
        {detailsData.map((detailsRow, index) => {
          return (
            <Row className="modalRow" style={{ marginTop: "8px" }} key={`details-row-${index}`}>
              <Col md={4}>
                <Typography className="modalBoldText">
                  {detailsRow.name}
                </Typography>
              </Col>
              <Col>
                <Typography className="modalTextValue">
                  {detailsRow.value}
                </Typography>
              </Col>
            </Row>
          );
        })}
      </Col>
    </Row>
  )
}

export default React.memo(Status);
