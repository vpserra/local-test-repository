import React from 'react';

import {
  Row,
  Col
} from "react-bootstrap";

import {
  Typography,
} from "@material-ui/core";

// import {
//   createStyles, 
//   makeStyles
// } from "@material-ui/styles";

// const useStyles = makeStyles(theme => createStyles({
//   previewChip: {
//     minWidth: 160,
//     maxWidth: 210
//   },
// }));

const Upload = () => {

  // const classes = useStyles();

  const toBase64 = file => new Promise((resolve, reject) => {
    
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);

  });


  const handle_upload_certificate = async (data_param) => {

    if(data_param.length !== 0){

      var file_uploaded = data_param[0];

      const base64_data = await toBase64(file_uploaded)
      const base64_fetched = await fetch(base64_data);
      
      console.log(base64_fetched)


      // var file_reader = new FileReader();

      // file_reader.readAsDataURL(file_uploaded);      


      // file_reader.onload = function() {
      //   data_blob.file_name = file_uploaded.name;
      //   data_blob.datablob = file_reader.result;
      // };

      // console.log({data_blob})
    }
  }

  return (
    <Row>
      <div className="upload-certificate-container"> 
        <Col>
          <Typography className="upload-certificate-text">
            Please upload a copy of your completion certificate. If you do not have this certificate, the driving school you completed the class with can provide it for you.
          </Typography>
        </Col>
        <Col>
          {/* <DropzoneArea
            showPreviews={true}
            showPreviewsInDropzone={false}
            useChipsForPreview
            previewGridProps={{container: { spacing: 1, direction: 'row' }}}
            previewChipProps={{classes: { root: classes.previewChip } }}
            previewText="Selected files"
            clearOnUnmount
            onChange={handle_upload_certificate}
            // onDelete={}
            filesLimit={1}
            dropzoneText={"Click or Drop file here to upload"}
            acceptedFiles={['.png, .jpeg. .gif, .jpg, .pdf']}
            maxFileSize={5242880}
          /> */}
        </Col>
      </div>
    </Row>
  )
}

export default React.memo(Upload);
