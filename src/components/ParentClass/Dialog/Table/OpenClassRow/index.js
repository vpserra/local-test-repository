import React, { useContext, Fragment } from 'react';

import {
  TableRow,
  TableCell,
  Button,
  styled 
} from '@material-ui/core';

import { tableCellClasses } from '@material-ui/core/TableCell';

import { ParentClassContext } from '../../../../../Context/ParentClass';

import { dateFormatted, timeFormatted } from '../../../../../utils/Appointments_functions';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    fontWeight: "700",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const OpenClassRow = ({open_class_row,index}) => {

  const { set_selected_data, setParent_class_actions } = useContext(ParentClassContext); 

  const handle_open_confirmation = async () => {
    await set_selected_data( prevState => ({
      ...prevState,
      open_class: open_class_row
    }));
    await setParent_class_actions( prevState => ({
      ...prevState,
      confirmation:{
        ...prevState.confirmation,
        open: true
      }
    }));
  }

  return (
    <Fragment>
      <StyledTableRow hover key={`parent-class-open-schedule-${index}`}>
        <StyledTableCell sx={{}}>
          {`
            ${dateFormatted(open_class_row.properties.schedule_date)} @ 
            ${timeFormatted(open_class_row.properties.schedule_start_time)} - 
            ${timeFormatted(open_class_row.properties.schedule_end_time)}
          `}
        </StyledTableCell>
        <StyledTableCell align="left">
          {open_class_row.properties.location_name || "TBD"}
        </StyledTableCell>
        <StyledTableCell align="center">
          <Button 
            variant="contained"
            className="btn-shadow green-tns"
            size="small"
            id="makeup-select-btn"
            onClick={() => handle_open_confirmation()}
          >
            Select
          </Button>
        </StyledTableCell>
      </StyledTableRow>
    </Fragment>
  )
}

export default React.memo(OpenClassRow);
