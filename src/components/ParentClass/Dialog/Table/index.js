import React, { useState } from 'react';
import { 
  Row, 
  Col 
} from "react-bootstrap";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Typography,
  styled
} from '@material-ui/core';

import { tableCellClasses } from '@material-ui/core/TableCell';

import Loader from "../../../Loader";
import OpenClassRow from './OpenClassRow';
import { dateFormatMDYHM } from '../../../../utils/Appointments_functions';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    fontWeight: "700",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));


const ScheduleTable = (props) =>{

  const { open_classes } = props;

  const sorted_open_classes = 
    open_classes ? 
      open_classes.sort((a,b) => {
        const a_variable = new Date(dateFormatMDYHM(a.properties.schedule_date, a.properties.schedule_start_time));
        const b_variable = new Date(dateFormatMDYHM(b.properties.schedule_date, b.properties.schedule_start_time));
        return a_variable - b_variable;
      }) : 
      [];

  const [table_data, setTable_data] = useState({
    table_headers: [
      {name: "Date", align: "left", minWidth: 50 },
      {name: "Location", align: "left", minWidth: 10 },
      {name: "Select", align: "center", minWidth: 10 }
    ],
    pagination: {
      page: 0,
      rowsPerPage: 10,
    }
  });

  const handleChangePage = (event, newPage) => {
    setTable_data( prevState => ({
      ...prevState,
      pagination: {
        ...prevState.pagination,
        page: newPage
      }
    }));
  };

  const handleChangeRowsPerPage = (event) => {
    setTable_data( prevState => ({
      ...prevState,
      pagination: {
        ...prevState.pagination,
        rowsPerPage: +event.target.value,
        page: 0
      }
    }));
  };

  return (
    <Row>
      <Col>
        <TableContainer>
          <Table
            size="small"
            aria-label="customized table"
            className="schedule-table"
          >
            <TableHead 
              sx={{ padding: "25px !important" }} 
              className="schedule-thead"
            > 
              <StyledTableRow key="schedule-thead-row">
                {
                  table_data.table_headers.map( (column, index) => {
                    return (
                      <StyledTableCell
                        align={column.align}
                        sx={{ minWidth: column.minWidth }}
                        key={`schedule-thead-column${index}`}
                      >
                        {column.name}
                      </StyledTableCell>
                    );
                  })
                }
              </StyledTableRow>
            </TableHead>
            <TableBody 
              className="schedule-tbody"
            >
              {
                open_classes ? 
                  sorted_open_classes.length === 0 ?
                    <StyledTableRow hover>
                      <StyledTableCell colSpan={3}>
                        <Typography sx={{textAlign: 'center', fontSize: 16}}>No schedules found</Typography>
                      </StyledTableCell>
                    </StyledTableRow> :
                    sorted_open_classes
                    .slice(table_data.pagination.page * table_data.pagination.rowsPerPage, table_data.pagination.page * table_data.pagination.rowsPerPage + table_data.pagination.rowsPerPage)
                    .map((open_class_row,index) => <OpenClassRow open_class_row={open_class_row} index={index} key={`ocr-${index}`}/> )                      
                :
                  <StyledTableRow>
                    <StyledTableCell colSpan={3}>
                      <Loader />
                    </StyledTableCell>
                  </StyledTableRow>
              }
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          className="schedule-table-pagination"
          labelDisplayedRows={({ from, to, count }) =>
            `Showing ${from} to ${to} of ${count} entries`
          }
          rowsPerPageOptions={[10]}
          component="div"
          count={open_classes?.length || 0}
          rowsPerPage={table_data.pagination.rowsPerPage}
          page={table_data.pagination.page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Col>
    </Row>
  )
}

export default React.memo(ScheduleTable);