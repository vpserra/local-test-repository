import React, { useState, Fragment, useContext, useEffect} from 'react';

import { 
  Row, 
  Col 
} from "react-bootstrap";

import { Button } from "@material-ui/core";

import {
  dateRangeArrayFormat,
  addMonths,
  search_timeFormat,
  epoch_date,
  format_timezone
} from "../../../../utils/mySchedule_functions";

import {
  fetch_open_parent_class
} from "../../../../utils/ParentClass/index";

import DateRange from "../../../Reusable/Filter/DateRange";
import Time from "../../../Reusable/Filter/Time";
import LocationProximity from "../../../Reusable/Filter/LocationProximity";

import { ParentClassContext } from '../../../../Context/ParentClass';

import ScheduleTable from "../Table";

const Schedule = ({type}) => {

  const { setParent_class_actions, set_selected_data } = useContext(ParentClassContext);

  //state for open classes
  const [ open_classes, set_open_classes ] = useState([{
    "id": "163782182",
    "properties": {
        "hs_createdate": "2021-05-20T13:53:50.481Z",
        "hs_lastmodifieddate": "2021-07-09T12:35:11.349Z",
        "hs_object_id": "163782182",
        "room_number": "https://www.google.com",
        "schedule_date": "2021-08-11",
        "schedule_end_time": "17:30",
        "schedule_location_name": "Live Virtual Room CR - Parent Class - 19",
        "schedule_name": "Session: 0 Live Virtual Zoom CR - Full Driver Education Course 19 - Parent Class",
        "schedule_start_time": "15:30",
        "session_number": "0"
    },
    "createdAt": "2021-05-20T13:53:50.481Z",
    "updatedAt": "2021-07-09T12:35:11.349Z",
    "archived": false,
    "class_properties": {
        "id": "144700992",
        "properties": {
            "available_": "Open",
            "class_name": "Live Virtual Zoom CR - Full Driver Education Course 19",
            "class_type": "Full",
            "classroom": "Live Virtual Room CR",
            "description": "A 30 hour driving course.",
            "duration": "30",
            "end_date": "2021-09-29",
            "end_time": "17:30",
            "hs_createdate": "2021-05-07T10:50:11.315Z",
            "hs_lastmodifieddate": "2021-07-20T14:26:30.815Z",
            "hs_object_id": "144700992",
            "license_testing": "false",
            "no__of_enrollees": "5",
            "no_of_session_per_day": "1",
            "online": "true",
            "seats_open": "10",
            "start_date": "2021-08-11",
            "start_time": "15:30",
            "state": "Massachusetts",
            "total_no_of_sessions": "15"
        },
        "createdAt": "2021-05-07T10:50:11.315Z",
        "updatedAt": "2021-07-20T14:26:30.815Z",
        "archived": false
    },
    "type": "class",
    "status": "scheduled",
    "testClass": "2021-07-26",
    "latest": true
},{
  "id": "163782181",
  "properties": {
      "hs_createdate": "2021-05-20T13:53:50.481Z",
      "hs_lastmodifieddate": "2021-07-09T12:35:11.349Z",
      "hs_object_id": "163782182",
      "room_number": "https://www.google.com",
      "schedule_date": "2021-08-10",
      "schedule_end_time": "17:30",
      "schedule_location_name": "Live Virtual Room CR - Parent Class - 19",
      "schedule_name": "Session: 0 Live Virtual Zoom CR - Full Driver Education Course 19 - Parent Class",
      "schedule_start_time": "15:30",
      "session_number": "0"
  },
  "createdAt": "2021-05-20T13:53:50.481Z",
  "updatedAt": "2021-07-09T12:35:11.349Z",
  "archived": false,
  "class_properties": {
      "id": "144700992",
      "properties": {
          "available_": "Open",
          "class_name": "Live Virtual Zoom CR - Full Driver Education Course 19",
          "class_type": "Full",
          "classroom": "Live Virtual Room CR",
          "description": "A 30 hour driving course.",
          "duration": "30",
          "end_date": "2021-09-29",
          "end_time": "17:30",
          "hs_createdate": "2021-05-07T10:50:11.315Z",
          "hs_lastmodifieddate": "2021-07-20T14:26:30.815Z",
          "hs_object_id": "144700992",
          "license_testing": "false",
          "no__of_enrollees": "5",
          "no_of_session_per_day": "1",
          "online": "true",
          "seats_open": "10",
          "start_date": "2021-08-11",
          "start_time": "15:30",
          "state": "Massachusetts",
          "total_no_of_sessions": "15"
      },
      "createdAt": "2021-05-07T10:50:11.315Z",
      "updatedAt": "2021-07-20T14:26:30.815Z",
      "archived": false
  },
  "type": "class",
  "status": "pending",
  "testClass": "2021-07-26",
  "latest": true
}]
);

  //useeffect runs on first load only
  // useEffect(() => {
  //   fetch_open_classes();
  // },[]);

  // handle for fetching open classes from API
  // const fetch_open_classes = async (state) => {
  //   const dataPromise = await fetch_open_parent_class(
  //     state,
  //     epoch_date(
  //       format_timezone(
  //         // ?.contact_information.hs_ip_timezone || ""
  //       )
  //     )
  //   );
  //   const checkedDataPromise = await (dataPromise === "error") ? recall_function(state) : dataPromise;
  //   await set_open_classes(checkedDataPromise);
  //   await setParent_class_actions(prevState => ({
  //     ...prevState,
  //     scheduling:{
  //       ...prevState.scheduling,
  //       isLoading: false
  //     }
  //   }));
  //   await set_selected_data( prevState => ({
  //     ...prevState,
  //     type: type
  //   }))
  // }

  //recall fetch_open_classes function if it encounters request timeout
  // const recall_function = (state) => {
  //   setTimeout(() => fetch_open_classes(state), 2000);
  // }

  //state for storing filter queries
  const [filter_query, set_filter_query] = useState ({
    initial: {
      startDate: null,
      endDate: null,
      time: null,
      proximity: 0,
    },
    selected: {
      startDate: null,
      endDate: null,
      time: null,
      proximity: 0
    },
    defaultValue: {
      minDate: dateRangeArrayFormat(new Date()),
      maxDate: dateRangeArrayFormat(addMonths(new Date(), 12)),
    }
  });

  //handle for what to set in filter state
  const handleChangeFilter = prop => e => {
    e.persist();

    const value_formatter = value => {
      if(prop === "time"){
        return search_timeFormat(value);
      }else if(prop === "startDate" || prop === "endDate"){
        if(prop === "endDate" && value < filter_query.initial.startDate){
          return filter_query.initial.startDate;
        }
        return dateRangeArrayFormat(value);
      }
      return value;
    }

    set_filter_query( prevState => ({
      ...prevState,
      initial: {
        ...prevState.initial,
        [prop]: value_formatter(e.target.value)
      }
    }));
  }

  return (
    <Fragment>
      <div className="filter-dialog-container">
        <Row>
          <Col lg={8} md={8} sm={12} xs={12}> 
            <Row>
              <DateRange filter_query={filter_query} handleChangeFilter={handleChangeFilter} />
            </Row>
            <Row>
              <Col lg={6} md={6} sm={6} xs={12}>
                <div className="filter-field-container">
                  <Time filter_query={filter_query} handleChangeFilter={handleChangeFilter} />
                </div>
              </Col>
              <Col lg={6} md={6} sm={6} xs={12}>
                <div className="filter-field-container filter-location">
                  <LocationProximity filter_query={filter_query} handleChangeFilter={handleChangeFilter} />
                </div>
              </Col>
            </Row>
          </Col>
          <Col lg={4} md={4} sm={12} xs={12}>
            <Row>
              <Col lg={12} md={12} sm={6} xs={12}>
                <div className="filter-field-container ">
                  <Button
                    className="green-tns btn-shadow"
                    id="filter-search-btn"
                    variant="contained"
                    fullWidth
                    // onClick={handleClick_Search}
                    disabled={Boolean(!open_classes || (!filter_query.initial.time && !filter_query.initial.startDate && !filter_query.initial.endDate && filter_query.initial.proximity === 0))} 
                  >
                    {
                      open_classes ? 
                        "Search" :
                        "Please wait"
                    }
                  </Button>
                </div>
              </Col>
              <Col lg={12} md={12} sm={6} xs={12}>
                <div className="filter-field-container">
                  <Button
                    variant="contained"
                    className="orange-tns btn-shadow"
                    id="filter-reset-btn"
                    fullWidth
                  >
                    Clear search
                  </Button>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <Row>
        <Col>
          <ScheduleTable open_classes={open_classes} />
        </Col>
      </Row> 
    </Fragment>
  )
}

export default React.memo(Schedule);
