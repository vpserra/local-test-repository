import React, { useContext } from 'react';

import {
  Button,
  Dialog,
  DialogTitle as MuiDialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Typography,
  CircularProgress
} from "@material-ui/core";

import {
  Close as CloseIcon
} from "@material-ui/icons";

import { ParentClassContext } from '../../../../Context/ParentClass';

import { 
  dateFormatted, 
  timeFormatted 
} from '../../../../utils/Appointments_functions';

const DialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <MuiDialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

const Confirmation = () => {

  const { selected_data, parent_class_actions, handle_close_dialog2, handle_reschedule_parent_class, handle_schedule_parent_class } = useContext(ParentClassContext);

  //handle if reschedule or resched a new parent class
  const handle_reschedule_by_type = () => {
    if(selected_data.type === "reschedule"){
      handle_reschedule_parent_class(
        selected_data.schedule.id,
        selected_data.open_class.id
      );
    }else{
      handle_schedule_parent_class(
        selected_data.open_class.id
      );
    }
  }

  return (
    <Dialog
      transitionDuration={400}
      onClick={handle_close_dialog2("confirmation")} 
      open={parent_class_actions.confirmation.open}
      maxWidth='sm'
      fullWidth
    >
      <DialogTitle onClose={handle_close_dialog2}>
        <Typography
          gutterBottom
          sx={{ fontWeight: 700, fontSize: '17px' }}
        >
          Date:{' '}
          <span className="open-class-confirmation-date">
          {`${dateFormatted(
            selected_data.open_class.properties.schedule_date,
          )} @ ${timeFormatted(selected_data.open_class.properties.schedule_start_time)} - ${
            timeFormatted(selected_data.open_class.properties.schedule_end_time)
          }`}
          </span>
        </Typography>
      </DialogTitle>
      <DialogContent dividers sx={{ padding: 2 }}>
        <Typography
          gutterBottom
          sx={{ fontWeight: 700, fontSize: '17px' }}
        >
          Location:
        </Typography>
        <Typography variant="body1" gutterBottom sx={{ fontWeight: 300, fontSize: '17px' }}>
          {selected_data.open_class.properties.location_name || "TBD"}
        </Typography>
      </DialogContent>
      <DialogActions sx={{ m: 0, padding: 1 }}>
        <Button
          autoFocus
          onClick={handle_reschedule_by_type}
          id="reschedule-parent-class-btn"
          className="btn-shadow green-tns"
        >
          {
            parent_class_actions.confirmation.isLoading ? (
              <span sx={{display: 'flex'}}>
                Please wait
                <CircularProgress 
                  color='inherit' 
                  size={16} 
                  sx={{color: '#ffffff', margin: 'auto auto auto 3px'}}
                />
              </span>
            ) : (
              selected_data.type === "reschedule" ? 'RESCHEDULE' : 'SCHEDULE'
            )
          }
        </Button>
        <Button 
          autoFocus 
          onClick={handle_close_dialog2("confirmation")} 
          className="orange-tns close-btn btn-shadow"
          id="close-btn">
          CLOSE
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default React.memo(Confirmation);
