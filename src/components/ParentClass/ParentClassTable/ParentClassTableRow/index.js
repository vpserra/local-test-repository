import React, { Fragment, useState, useContext } from 'react';

import {
  TableRow,
  TableCell,
  Button,
  Menu,
  MenuItem,
  styled
} from "@material-ui/core";

import { tableCellClasses } from '@material-ui/core/TableCell';

import { withStyles } from '@material-ui/styles';

import {
  Visibility as VisibilityIcon,
  EventNote as EventNoteIcon,
  Block as BlockIcon
} from "@material-ui/icons";

import {
  dateFormatted,
  timeFormatted,
  urlChecker
} from '../../../../utils/Appointments_functions';

import { ParentClassContext } from "../../../../Context/ParentClass";

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5'
  }
})(props => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center'
    }}
    PaperProps={{
      border: '1px solid #d3d4d5'
    }}
    {...props}
  />
));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    fontWeight: "700",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const ParentClassTableRow = ({data,index}) => {
  
  const { handle_modal_content, set_selected_data } = useContext(ParentClassContext);

  //Change menu state and handles
  const [menuAnchorEl, setMenuAnchorEl] = useState(null);
  const handleOpen_menuAnchor = e => setMenuAnchorEl(e.currentTarget);
  const handleClose_menuAnchor = () => setMenuAnchorEl(null);

  const handle_status_button = (value) => {
    return(
      <Fragment>
        <Button
          variant="contained"
          className={`btn-shadow ${handle_status_button_design("class",value.status.toLowerCase())}`}
          fullWidth
          id={`${handle_status_button_design("id",value.status.toLowerCase())}-status-btn`}
          onClick={() => handle_choose_schedule(4)}
        >
          {handle_status_button_text(value.status.toLowerCase())}
        </Button>
        {
          (["scheduled", "absent", "cancelled", "no show"].includes(value.status.toLowerCase())) ?
            (["absent", "cancelled", "no show"].includes(value.status.toLowerCase())) ?
              <Button
                fullWidth
                className={`btn-shadow ${handle_status_button_design("class","schedule")}`}
                id="reschedule-status-btn"
                variant="contained"
                onClick={() => handle_choose_schedule(3)}
              >
                {handle_status_button_text("schedule")}
              </Button> :
              <Fragment>
                <Button
                  fullWidth
                  className="btn-shadow orange-tns"
                  variant="contained"
                  onClick={handleOpen_menuAnchor}
                  id="change-status-btn"
                >
                  {handle_status_button_text("change")}
                </Button>
                <StyledMenu
                  id="parent-class-change-menu"
                  anchorEl={menuAnchorEl}
                  keepMounted
                  open={Boolean(menuAnchorEl)}
                  onClose={handleClose_menuAnchor}
                >
                    <MenuItem>
                      <Button
                        className="btn-shadow green-tns"
                        fullWidth
                        id="reschedule-status-btn"
                        variant="contained"
                        onClick={() => handle_choose_schedule(3,handleClose_menuAnchor)}
                      >
                        <EventNoteIcon id="options-menu-icon" />
                        Reschedule Class
                      </Button>
                    </MenuItem>
                  <MenuItem>
                    <Button
                      fullWidth
                      id="cancel-status-btn"
                      variant="contained"
                      className="btn-shadow orange-tns"
                      onClick={() => handle_choose_schedule(5,handleClose_menuAnchor)}
                    >
                      <BlockIcon id="options-menu-icon" />
                        Cancel Class
                    </Button>
                  </MenuItem>
                </StyledMenu> 
              </Fragment>  
          : ""
        }
      </Fragment>
    )
  }

  const handle_status_button_design = (type, value) => {
    if(type === "class"){
      switch (value) {
        case "scheduled":
        case "passed":
        case "present":
        case "schedule":
          return "green-tns";
        case "cancelled":
        case "cancel":
        case "no show":
        case "change":
        case "absent":
          return "orange-tns";
        default:
          return ""
      } 
    }else{
      switch (value) {
        case "no show":
          return "noshow";
        case "":
          return "pending";
        default:
          return value;
      }
    }
  }

  const handle_status_button_text = (value) => {
    switch (value) {
      case "":
        return "pending"
      case "passed":
        return "session Passed"
      case "schedule":
        return "schedule makeup"
      case "makeup":
        return "reschedule class"
      case "cancel":
        return "cancel class"
      default:
        return value;
    }
  }

  const handle_choose_schedule = async(value,close_anchor_cb) => {
    if(close_anchor_cb) close_anchor_cb();

    await set_selected_data( prevState => ({
      ...prevState,
      schedule: data
    }));
    await handle_modal_content(value)
  }

  return (
    <StyledTableRow hover key={`parent-class-scheduled-${index}`}>
      <StyledTableCell align="center">
      {
        data.properties.schedule_date
          ? `${dateFormatted(data.properties.schedule_date)} `
          : ''
      } 
      {
        data.properties.schedule_start_time
          ? timeFormatted(data.properties.schedule_start_time) +
            ' - '
          : ''
      } 
      {
        data.properties.schedule_end_time
          ? timeFormatted(data.properties.schedule_end_time)
          : ''
      }
      </StyledTableCell>
      <StyledTableCell align="center">
        {handle_status_button(data)}
      </StyledTableCell>
      <StyledTableCell align="center">
        {urlChecker(data.properties.room_number)}
      </StyledTableCell>
      <StyledTableCell align="center">
        <Button
          size="small"
          className="btn-shadow green-tns"
          id="parent-class-view-details-btn"
          variant="contained"
          // onClick={handleOpenDetails}
          onClick={() => handle_choose_schedule(6)}
        >
          <VisibilityIcon />
        </Button>
      </StyledTableCell>
    </StyledTableRow>    
  )
}

export default React.memo(ParentClassTableRow);
