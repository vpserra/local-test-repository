import React, { useState, Fragment, useContext } from 'react';

import {
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
  Typography,
  styled
} from "@material-ui/core";
import { tableCellClasses } from '@material-ui/core/TableCell';

import { ParentClassContext } from "../../../Context/ParentClass";
import ParentClassTableRow from "./ParentClassTableRow";
import Loader from "../../Loader";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    fontWeight: "700",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const ParentClassTable = () => {

  const { parent_class_data, sorted_parent_class_data } = useContext(ParentClassContext);

  const [table_data, setTable_data] = useState({
    table_headers: [
      {name: "Date/Time", align: "center", minWidth: 50 },
      {name: "Status", align: "center", minWidth: 140 },
      {name: "Room Number", align: "center", minWidth: 50 },
      {name: "Detail", align: "center", minWidth: 50 }
    ],
    pagination: {
      page: 0,
      rowsPerPage: 25,
    }
  });

  const handleChangePage = (event, newPage) => {
    setTable_data( prevState => ({
      ...prevState,
      pagination: {
        ...prevState.pagination,
        page: newPage
      }
    }));
  };

  const handleChangeRowsPerPage = (event) => {
    setTable_data( prevState => ({
      ...prevState,
      pagination: {
        ...prevState.pagination,
        rowsPerPage: +event.target.value,
        page: 0
      }
    }));
  };

  return (
    <Fragment>
      <TableContainer>
        <Table border={1} id="parent-class-table">
          <TableHead sx={{ padding: "25px !important" }} className="parent-class-thead">
            <StyledTableRow key="parent-class-thead-row">
              {
                table_data.table_headers.map( (column, index) => {
                  return (
                    <StyledTableCell
                      align={column.align}
                      sx={{ minWidth: column.minWidth }}
                      key={`parent-class-thead-column${index}`}
                    >
                      {column.name}
                    </StyledTableCell>
                  );
                })
              }
            </StyledTableRow>
          </TableHead>
          <TableBody className="parent-class-tbody">
            {
              parent_class_data ? 
                parent_class_data.length === 0 ?
                  <StyledTableRow key="no-parent-class-schedule" hover>
                    <StyledTableCell colSpan={4}>
                      <Typography variant="h6" sx={{fontSize: 16}} align="center">No schedules found.</Typography>
                    </StyledTableCell>
                  </StyledTableRow>
                  :
                  sorted_parent_class_data
                  .slice(table_data.pagination.page * table_data.pagination.rowsPerPage, table_data.pagination.page * table_data.pagination.rowsPerPage + table_data.pagination.rowsPerPage)
                  .map((row, index) => <ParentClassTableRow data={row} index={index} key={`pctr-${index}`}/>)
                :
                <StyledTableRow key="loading-parent-class-schedule" hover>
                  <StyledTableCell colSpan={4}>
                    <Loader />
                  </StyledTableCell>
                </StyledTableRow>
            }
            
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        className="parent-class-table-pagination"
        labelDisplayedRows={({ from, to, count }) =>
          `Showing ${from} to ${to} of ${count} entries`
        }
        rowsPerPageOptions={[10,25,50,100]}
        component="div"
        count={parent_class_data?.length || 0}
        rowsPerPage={table_data.pagination.rowsPerPage}
        page={table_data.pagination.page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Fragment>
  )
}

export default React.memo(ParentClassTable);
