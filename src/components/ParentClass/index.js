import React, { useState, useEffect } from 'react';
import { Row, Col } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Card,
  CardContent,
  Typography,
  Button
} from '@material-ui/core';

import { ParentClassContext } from "../../Context/ParentClass";

import {
  reschedMakeupClass,
  handle_cancel_schedule
} from "../../utils/mySchedule_apiCall";
import {
  schedule_parent_class,
  fetch_scheduled_parent_class
} from "../../utils/ParentClass";
import {
  dateFormatMDYHM
} from "../../utils/Appointments_functions";

import ParentClassTable from "./ParentClassTable";
import ParentClassDialog from './Dialog';
import Confirmation from "./Dialog/Confirmation";
import Alert from "../Reusable/Alert";

import { Class as ClassIcon } from "@material-ui/icons";

const ParentClass = () => {

  const [parent_class_data, set_parent_class_data] = useState([{
    "id": "163782182",
    "properties": {
        "hs_createdate": "2021-05-20T13:53:50.481Z",
        "hs_lastmodifieddate": "2021-07-09T12:35:11.349Z",
        "hs_object_id": "163782182",
        "room_number": "https://www.google.com",
        "schedule_date": "2021-08-11",
        "schedule_end_time": "17:30",
        "schedule_location_name": "Live Virtual Room CR - Parent Class - 19",
        "schedule_name": "Session: 0 Live Virtual Zoom CR - Full Driver Education Course 19 - Parent Class",
        "schedule_start_time": "15:30",
        "session_number": "0"
    },
    "createdAt": "2021-05-20T13:53:50.481Z",
    "updatedAt": "2021-07-09T12:35:11.349Z",
    "archived": false,
    "class_properties": {
        "id": "144700992",
        "properties": {
            "available_": "Open",
            "class_name": "Live Virtual Zoom CR - Full Driver Education Course 19",
            "class_type": "Full",
            "classroom": "Live Virtual Room CR",
            "description": "A 30 hour driving course.",
            "duration": "30",
            "end_date": "2021-09-29",
            "end_time": "17:30",
            "hs_createdate": "2021-05-07T10:50:11.315Z",
            "hs_lastmodifieddate": "2021-07-20T14:26:30.815Z",
            "hs_object_id": "144700992",
            "license_testing": "false",
            "no__of_enrollees": "5",
            "no_of_session_per_day": "1",
            "online": "true",
            "seats_open": "10",
            "start_date": "2021-08-11",
            "start_time": "15:30",
            "state": "Massachusetts",
            "total_no_of_sessions": "15"
        },
        "createdAt": "2021-05-07T10:50:11.315Z",
        "updatedAt": "2021-07-20T14:26:30.815Z",
        "archived": false
    },
    "type": "class",
    "status": "absent",
    "testClass": "2021-07-26",
    "latest": true
},{
  "id": "163782181",
  "properties": {
      "hs_createdate": "2021-05-20T13:53:50.481Z",
      "hs_lastmodifieddate": "2021-07-09T12:35:11.349Z",
      "hs_object_id": "163782182",
      "room_number": "https://www.google.com",
      "schedule_date": "2021-08-10",
      "schedule_end_time": "17:30",
      "schedule_location_name": "Live Virtual Room CR - Parent Class - 19",
      "schedule_name": "Session: 0 Live Virtual Zoom CR - Full Driver Education Course 19 - Parent Class",
      "schedule_start_time": "15:30",
      "session_number": "0"
  },
  "createdAt": "2021-05-20T13:53:50.481Z",
  "updatedAt": "2021-07-09T12:35:11.349Z",
  "archived": false,
  "class_properties": {
      "id": "144700992",
      "properties": {
          "available_": "Open",
          "class_name": "Live Virtual Zoom CR - Full Driver Education Course 19",
          "class_type": "Full",
          "classroom": "Live Virtual Room CR",
          "description": "A 30 hour driving course.",
          "duration": "30",
          "end_date": "2021-09-29",
          "end_time": "17:30",
          "hs_createdate": "2021-05-07T10:50:11.315Z",
          "hs_lastmodifieddate": "2021-07-20T14:26:30.815Z",
          "hs_object_id": "144700992",
          "license_testing": "false",
          "no__of_enrollees": "5",
          "no_of_session_per_day": "1",
          "online": "true",
          "seats_open": "10",
          "start_date": "2021-08-11",
          "start_time": "15:30",
          "state": "Massachusetts",
          "total_no_of_sessions": "15"
      },
      "createdAt": "2021-05-07T10:50:11.315Z",
      "updatedAt": "2021-07-20T14:26:30.815Z",
      "archived": false
  },
  "type": "class",
  "status": "pending",
  "testClass": "2021-07-26",
  "latest": true
}]);

  useEffect(() => {
    //handle_scheduled_parent_class(contact_id);
  },[])

  const sorted_parent_class_data = 
    parent_class_data ?
    parent_class_data.sort((a,b) => {
      const a_variable = new Date(dateFormatMDYHM(a.properties.schedule_date, a.properties.schedule_start_time));
      const b_variable = new Date(dateFormatMDYHM(b.properties.schedule_date, b.properties.schedule_start_time));
      return a_variable - b_variable;
    }) : 
    [];

  const [ parent_class_actions, setParent_class_actions ] = useState({
    modal_content: 0,
    schedule_dialog: false,
    confirmation: {
      open: false,
      isLoading: false
    },
    scheduling: {
      isLoading: true,
      isSearching: false
    },
    cancellation: {
      isLoading: false,
    },
    alert: {
      open: false,
      status: "",
      color: "",
      text: "",
      text1: "",
      text2: ""
    }
  });

  const activateAlert = async (message, text1, text2) => {
    if(message === "error"){
      await setParent_class_actions( prevState => ({ 
        ...prevState,
        alert:{
          open: true,
          status: "error", 
          color: '#cb5b5f', 
          text: "Error!", 
          text1: "An error has occured!", 
          text2: "Please try again."
        }
      }));
    }else{
      await setParent_class_actions( prevState => ({ 
        ...prevState,
        alert:{ 
          open: true,
          status: "success", 
          color: '#28d279', 
          text: "Success!", 
          text1: text1, 
          text2: text2
        }
      }));
    }
  }

  const handle_close_alert = () => (
    setParent_class_actions( prevState => ({ 
      ...prevState,
      alert:{
        open: false,
        status: "", 
        color: '', 
        text: "", 
        text1: "", 
        text2: ""
      }
    }))
  );

  const handle_close_dialog2 = (prop) => () => {
    /*
      setParent_class_actions({
        modal_content: 0,
        main_dialog: {
          open: false,
          isLoading: false
        },
        confirmation: {
          open: false,
          isLoading: false
        },
      });
      setParent_class_actions((prevState) => ({
        ...prevState,
        [prop]: {
          open: false,
          isLoading: false
        }
      }));
    */
    setParent_class_actions(prevState => ({
      ...prevState,
      confirmation: {
        open: false,
        isLoading: false
      }
    }));
  }

  //Handle for closing the dialog
  const handle_close_dialog = () => {
    set_selected_data({
      type: null,
      schedule: null,
      open_class: null
    });
    setParent_class_actions( prevState => ({
      ...prevState,
      schedule_dialog: false,
      modal_content: 0
    }));
  }

  //Handle for changing modal content
  const handle_modal_content = (value) => {
    setParent_class_actions( prevState => ({
      ...prevState,
      schedule_dialog: true,
      modal_content: value
    }));
  }

  //selected data
  const [selected_data, set_selected_data] = useState({
    type: null,
    schedule: null,
    open_class: null
  });

  //handle for fetching scheduled parent class
  const handle_scheduled_parent_class = async (contact_id, schedule_id) => {
    const response = await fetch_scheduled_parent_class(contact_id);
    if(response === "error"){
      console.error(response);
    }else{
      set_parent_class_data(response);
    }
  }

  //handle for uploading certificate

  //handle for scheduling new parent class
  const handle_schedule_parent_class = async (contact_id, schedule_id) => {
    const response = await schedule_parent_class(contact_id, schedule_id);
    if(response === "error"){
      await activateAlert(response, "", "");
      await handle_close_dialog();
    }else{
      await set_parent_class_data(prevState => ({
        ...prevState,
        ...response
      }));
      await handle_close_dialog();
      await activateAlert(response, "Your class has successfully ", "scheduled.");
    }
  }

  //handle for rescheduling parent class
  const handle_reschedule_parent_class = async(contact_id, old_id, new_id) => {
    const response = await reschedMakeupClass(contact_id, old_id, new_id);
    if(response === "error"){
      await activateAlert(response, "", "");
      await handle_close_dialog();
    }else{
      await setParent_class_actions({
        modal_content: 0,
        schedule_dialog: false,
        confirmation: {
          open: false,
          isLoading: false
        },
        scheduling: {
          isLoading: true,
          isSearching: false
        }
      });
      await handle_close_dialog();
      await activateAlert(response, "Your class has successfully ", "rescheduled.");
    }
  }

  //handle for cancelling class
  const handle_cancel_parent_class = async (contact_id, schedule_id) => {
    const response = await handle_cancel_schedule(contact_id, schedule_id);
    console.log(response)
    if(response === "error"){
      await activateAlert(response, "", "");
      await handle_close_dialog();
    }else{
      await set_parent_class_data( prevState => {
        const updatedState = prevState.filter( prevState_row => prevState_row.id !== schedule_id);
        return updatedState;
      });
      await handle_close_dialog();
      await activateAlert(response, "You have successfully cancelled", "this class");
    }
  }

  return (
    <ParentClassContext.Provider 
      value={{ 
        setParent_class_actions,
        parent_class_actions, //for dialog actions state
        handle_modal_content, //dialog content handle
        handle_close_dialog, //handle for closing the dialog
        handle_close_dialog2, 
        parent_class_data, //state for scheduled parent class
        sorted_parent_class_data, //sorted state of scheduled parent class
        selected_data, //selected row data for scheduled and open classes row
        set_selected_data,
        handle_schedule_parent_class, //handle for scheduling new parent class
        handle_reschedule_parent_class, //handle for rescheduling to a new parent class schedule
        handle_cancel_parent_class, //cancel parent class schedule
        activateAlert, //handle for activating alert
      }}
    >
      <Row>
        <Col>
          <Card>
            <CardContent>
              <Row>
                <Col>
                  <div className="parent-class-header row-container">
                    <ClassIcon />
                    <Typography variant="h6">
                      Parent Class Schedule
                    </Typography>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col>
                      <div className="parent-class-add-schedule row-container">
                        <Card 
                          variant="outlined"
                          elevation={0} 
                          className="parent-class-add-schedule-card"
                        >
                          <Typography className="parent-class-add-schedule-text">
                            Schedule your Parent Class with <span>The Next Street</span>.
                          </Typography>
                        </Card>
                        <Button 
                          variant="contained" 
                          id="parent-class-add-schedule-btn" 
                          className="green-tns" 
                          fullWidth
                          disableElevation
                          onClick={() => handle_modal_content(0)}
                        >
                          Add Schedule
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col>
                  <ParentClassTable />
                </Col>
              </Row>
            </CardContent>
          </Card>
        </Col>
      </Row>
      <ParentClassDialog />
      { parent_class_actions.confirmation.open && <Confirmation />}
      { parent_class_actions.alert.open && <Alert handleClose={handle_close_alert} open={parent_class_actions.alert.open} alertProps={parent_class_actions.alert} />}
    </ParentClassContext.Provider>
  )
}

export default React.memo(ParentClass);
