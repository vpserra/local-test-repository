import './App.scss';
import ParentClass from './components/ParentClass/index';
import React from 'react';

function App() {
  return (
    <div sx={{margin: 20}}>
      <ParentClass />
    </div>
  );
}

export default React.memo(App);
